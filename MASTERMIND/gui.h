#ifndef GRACZE
#define GRACZE

#include <iostream>
#include "cstdlib"
#include "windows.h"
#include "stdlib.h"
#include "conio.h"
#include "iomanip"

using namespace std;

class gui {

public:
	
	void info_gry();
	int wybierz_tryb();
	void info_tryb();
};
void gui::info_gry() {
	HANDLE hOut;
	cout << setw(46) << setfill(' ') << "GRA MASTERMIND" << endl << endl;
	
	cout << setw(53) << setfill(' ') << "Oznaczenia kolorow i zasady" << endl << endl;

	hOut = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(hOut, FOREGROUND_GREEN | FOREGROUND_INTENSITY);
	cout << setw(15) << setfill(' ') << "Z-zielony ";
	
	SetConsoleTextAttribute(hOut, FOREGROUND_BLUE | FOREGROUND_INTENSITY);
	cout << "N-niebieski ";
	
	SetConsoleTextAttribute(hOut, FOREGROUND_RED | FOREGROUND_INTENSITY);
	cout << "C-czerowy ";
	
	SetConsoleTextAttribute(hOut, FOREGROUND_RED| FOREGROUND_BLUE);
	cout << "F - fioletowy ";
	
	SetConsoleTextAttribute(hOut, 3 | FOREGROUND_INTENSITY);
	cout << "M-morski ";

	SetConsoleTextAttribute(hOut, FOREGROUND_RED| FOREGROUND_GREEN|FOREGROUND_INTENSITY);
	cout << "P-pomaranczowy " << endl << endl;

	SetConsoleTextAttribute(hOut, FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_RED); hOut = GetStdHandle(STD_OUTPUT_HANDLE);
	cout << "x oznacza ze kolor i polozenie jednego ze znakow si� sie zgadza" << endl;
	cout << "o oznacza ze kolor jednego ze znakow sie zgadza ale polozenie nie" << endl << endl;
	cout << "Masz 10 rund na odgadniecie ukrytego kodu" << endl;
	cout << "POWODZENIA!" << endl << endl;
}

void gui::info_tryb() {
	cout << endl << endl;
	cout << "Wybierz tryb gry" << endl;
	cout << "Nacisnij liczbe odpowiadajaca Twojemu wyborowi" << endl;
	cout << "1 - gra z komputerem" << endl;
	cout << "2 - gra z 2 graczami" << endl;
	cout << "3 - gra z 3 graczami" << endl;
	cout << "4-Wyjdz z gry" << endl;
}
int gui::wybierz_tryb() {
	
	int wybor;
	do {
		cin >> wybor;
		if (wybor < 1 || wybor>4) {
			cout << "Wprowadz poprawna liczbe!" << endl;
		}
	} while (wybor < 1 || wybor>4);
	return wybor;
}

#endif // !GRACZE
