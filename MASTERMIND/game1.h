#ifndef GAME1
#define GAME1
#include <iostream>
#include "cstdlib"
#include "windows.h"
#include "stdlib.h"
#include "conio.h"
#include "iomanip"
#include "fstream"
using namespace std;

class gracz  {
protected:
	string nazwa_gracza1;
	string nazwa_gracza2;
	string nazwa_gracza3;
public:
	gracz();

	void set_nick(string nick, int liczba_gracza);
	string get_nick(int liczba_gracza);
	void wypisz_nick(int liczba_gracza);
	void zapisz_nicki();

};

class gra : public gracz  {
public:
	char baza[6] = { 'Z','N','C','F','M','P' };
	char kod[4];
	char tab_[4];
	char zgadywanie[4];

	char gkod[4], gkod2[4], gkod3[4];
	char gtab_[4], gtab_2[4], gtab_3[4];
	char gzgadywanie[4], gzgadywanie2[4], gzgadywanie3[4];

	void utworz_kod(char baza[6], char kod[4]);
	void wypisz_kod4(char kod[4]);
	void uzupelnij_(char kod[4]);
	void uzupelnij_xo(char tab[4], int x, int o);
	void wypisz_(char tab[4]);

	void gra_z_komputerem(char kod[4], char tab_[4], char zgadywanie[4]);

	void gra_2graczy(char gkod[4], char gkod2[4], char gtab_[4], char gtab_2[4], char gzgadywanie[4], char gzgadywanie2[4]);
	void gra_2graczy_przeciw_sobie(char gtab_[4], char gtab_2[4], char gzgadywanie[4], char gzgadywanie2[4]);
};

#endif // !GAME
