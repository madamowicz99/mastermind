#include "game1.h"

using namespace std;
//Funkcja tworzy kod do zgadywania
void gra::utworz_kod(char baza[6], char kod[4]) { 
	for (int i = 0; i < 4; i++) {
		int kolor = rand() % 5;
		kod[i] = baza[kolor];
	}

}
//Funkcja wypisuje kod
void gra::wypisz_kod4(char kod[4]) {
	HANDLE hOut;
	for (int i = 0; i < 4; i++) {
		if (kod[i] == 'Z') {
			hOut = GetStdHandle(STD_OUTPUT_HANDLE);
			SetConsoleTextAttribute(hOut, FOREGROUND_GREEN | FOREGROUND_INTENSITY);
			cout << kod[i] << " ";
		}
		if (kod[i] == 'N') {
			hOut = GetStdHandle(STD_OUTPUT_HANDLE);
			SetConsoleTextAttribute(hOut, FOREGROUND_BLUE | FOREGROUND_INTENSITY);
			cout << kod[i] << " ";
		}
		if (kod[i] == 'C') {
			hOut = GetStdHandle(STD_OUTPUT_HANDLE);
			SetConsoleTextAttribute(hOut, FOREGROUND_RED | FOREGROUND_INTENSITY);
			cout << kod[i] << " ";
		}
		if (kod[i] == 'F') {
			hOut = GetStdHandle(STD_OUTPUT_HANDLE);
			SetConsoleTextAttribute(hOut, FOREGROUND_RED | FOREGROUND_BLUE);
			cout << kod[i] << " ";
		}
		if (kod[i] == 'M') {
			hOut = GetStdHandle(STD_OUTPUT_HANDLE);
			SetConsoleTextAttribute(hOut, 3 | FOREGROUND_INTENSITY);
			cout << kod[i] << " ";
		}
		if (kod[i] == 'P') {
			hOut = GetStdHandle(STD_OUTPUT_HANDLE);
			SetConsoleTextAttribute(hOut, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_INTENSITY);
			cout << kod[i] << " ";
		}

	}
	hOut = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(hOut, FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_RED); hOut = GetStdHandle(STD_OUTPUT_HANDLE);
}

//Uzupelnienie znakami tablicy podpowiedzi

void gra::uzupelnij_(char tab[4]) {
	for (int i = 0; i < 4; i++) {
		tab[i] = '_';
	}
}
void gra::uzupelnij_xo(char tab[4], int x, int o) {
	for (int i = 0; i < x; i++) {
		tab[i] = 'x';
	}
	for (int i = x; i < o; i++) {
		tab[i] = 'o';
	}
}

void gra::wypisz_(char tab[4]) {
	for (int i = 0; i < 4; i++) {
		cout << tab[i] << " ";
	}
}

void gra::gra_z_komputerem(char kod[4], char tab_[4], char zgadywanie[4]) {


	int liczba_rundy = 1;
	ifstream plik;
	string sprawdzenie;
	char tab[1000];
	int licznik = 0;
	plik.open("odpowiedzi.txt");
	if (plik) {
		int wyb;
		cout << "Znaleziono niedokonczona gre. Czy chcesz wczytac?" << endl;
		cout << "1-TAK 2-NIE" << endl;
		cin >> wyb;
		if (wyb == 1) {
			plik.close();
			plik.open("odpowiedzi.txt", ios::in);
			while (!plik.eof()) {
				char pom;
				plik.get(pom);
				tab[licznik] = pom;
				licznik++;
			}
			licznik = licznik - 2;
			sprawdzenie = tab[0];
		}
		else {
			plik.close();
			remove("odpowiedzie.txt");
			sprawdzenie = "X";
		}
	}
	else {
		plik.close();
		plik.open("odpowiedzi.txt", ios::app);
		sprawdzenie = "X";
	}

	if (sprawdzenie == "Z" || sprawdzenie == "N" || sprawdzenie == "C" || sprawdzenie == "F" || sprawdzenie == "M" || sprawdzenie == "P") {

		int nr_ost_rundy;
		nr_ost_rundy = (licznik + 1) / 4;
		liczba_rundy = nr_ost_rundy;
		plik.close();
		/*remove("odpowiedzi.txt");*/
		fstream plik1;
		plik1.open("odpowiedzi.txt", ios::app);

		do {



			if (liczba_rundy == nr_ost_rundy) {
				int ilosc_x = 0;
				int ilosc_o = 0;
				int licz_pom = 3;
				for (int i = 0; i < 4; i++) {
					zgadywanie[i] = tab[licznik - licz_pom];
					licz_pom--;
				}

				for (int i = 0; i < 4; i++) {
					for (int j = 0; j < 4; j++) {
						if (zgadywanie[j] == kod[i] && j == i) {
							ilosc_x++;
						}

					}
				}


				for (int i = 0; i < 4; i++) {
					for (int j = 0; j < 4; j++) {
						if (zgadywanie[j] != kod[j]) {
							if (zgadywanie[i] != kod[i]) {
								if (zgadywanie[j] == kod[i] && j != i) {
									ilosc_o++;
									j = 4;
								}
							}
						}
					}
				}
				cout << "RUNDA:" << liczba_rundy << endl;
				wypisz_kod4(zgadywanie);
				uzupelnij_xo(tab_, ilosc_x, ilosc_o);
				cout << " ";
				wypisz_(tab_);
				cout << endl << endl;
				liczba_rundy++;
			}

			cout << "RUNDA:" << liczba_rundy << endl;

			cout << "wpisz kod" << endl;
			int ilosc_x = 0;
			int ilosc_o = 0;
			for (int i = 0; i < 4; i++) {
				char znak;
				do {
					cin >> znak;
				} while (znak != 'Z' && znak != 'N' && znak != 'C' && znak != 'F' && znak != 'M' && znak != 'P');
				zgadywanie[i] = znak;
				plik1 << znak;

			}
			plik1.close();
			for (int i = 0; i < 4; i++) {
				for (int j = 0; j < 4; j++) {
					if (zgadywanie[j] == kod[i] && j == i) {
						ilosc_x++;
					}

				}
			}

			for (int i = 0; i < 4; i++) {
				for (int j = 0; j < 4; j++) {
					if (zgadywanie[j] != kod[j]) {
						if (zgadywanie[i] != kod[i]) {
							if (zgadywanie[j] == kod[i] && j != i) {
								ilosc_o++;
								j = 4;
							}
						}
					}
				}
			}
			wypisz_kod4(zgadywanie);
			uzupelnij_xo(tab_, ilosc_x, ilosc_o);
			cout << " ";
			wypisz_(tab_);
			cout << endl << endl;

			liczba_rundy++;
			if (ilosc_x == 4) {
				cout << endl << "GRATULACJE ODGADLES KOD!" << endl;
				if (liczba_rundy != 2) {
					cout << "Udalo Ci sie w " << liczba_rundy - 1 << " rundach";

				}			if (liczba_rundy == 2) {
					cout << "Udalo Ci sie w 1 runde";
				}
				cout << endl;
				liczba_rundy = 11;
				plik1.close();
				remove("odpowiedzi.txt");
			}

			if (ilosc_x != 4 && liczba_rundy == 11) {
				cout << "Niestety sie nie udalo" << endl;
				cout << "Kod byl nastepujacy : ";
				wypisz_kod4(kod);
				cout << endl;
				plik1.close();
				remove("odpowiedzi.txt");
			}
		} while (liczba_rundy != 11);

	}
	else {
		//pusty
		plik.close();
		fstream plik2;

		do {

			cout << "RUNDA:" << liczba_rundy << endl;

			cout << "wpisz kod" << endl;
			int ilosc_x = 0;
			int ilosc_o = 0;

			for (int i = 0; i < 4; i++) {
				char znak;
				do {
					cin >> znak;
				} while (znak != 'Z' && znak != 'N' && znak != 'C' && znak != 'F' && znak != 'M' && znak != 'P');
				zgadywanie[i] = znak;
				plik2.open("odpowiedzi.txt", ios::app);
				plik2 << znak;
				plik2.close();
			}

			for (int i = 0; i < 4; i++) {
				for (int j = 0; j < 4; j++) {
					if (zgadywanie[j] == kod[i] && j == i) {
						ilosc_x++;
					}

				}
			}

			for (int i = 0; i < 4; i++) {
				for (int j = 0; j < 4; j++) {
					if (zgadywanie[j] != kod[j]) {
						if (zgadywanie[i] != kod[i]) {
							if (zgadywanie[j] == kod[i] && j != i) {
								ilosc_o++;
								j = 4;
							}
						}
					}
				}
			}
			wypisz_kod4(zgadywanie);
			uzupelnij_xo(tab_, ilosc_x, ilosc_o);
			cout << " ";
			wypisz_(tab_);
			cout << endl << endl;

			liczba_rundy++;
			if (ilosc_x == 4) {
				cout << endl << "GRATULACJE ODGADLES KOD!" << endl;
				if (liczba_rundy != 2) {
					cout << "Udalo Ci sie w " << liczba_rundy - 1 << " rundach";

				}			if (liczba_rundy == 2) {
					cout << "Udalo Ci sie w 1 runde";
				}
				cout << endl;
				plik2.close();
				remove("odpowiedzi.txt");
				liczba_rundy = 11;
			}

			if (ilosc_x != 4 && liczba_rundy == 11) {
				cout << "Niestety sie nie udalo" << endl;
				cout << "Kod byl nastepujacy : ";
				wypisz_kod4(kod);
				plik2.close();
				remove("odpowiedzi.txt");
				cout << endl;
			}
		} while (liczba_rundy != 11);

	}


}

gracz::gracz() {
	this->nazwa_gracza1 = "gracz1";
	this->nazwa_gracza2 = "gracz2";
	this->nazwa_gracza3 = "gracz3";
}

void gracz::set_nick(string nick, int liczba_gracza) {
	if (liczba_gracza == 1) {
		this->nazwa_gracza1 = nick;
	}
	if (liczba_gracza == 2) {
		this->nazwa_gracza2 = nick;
	}
	if (liczba_gracza == 3) {
		this->nazwa_gracza3 = nick;
	}

}

string gracz::get_nick(int liczba_gracza) {
	if (liczba_gracza == 1) {
		return this->nazwa_gracza1;
	}
	if (liczba_gracza == 2) {
		return this->nazwa_gracza1;
	}
	if (liczba_gracza == 3) {
		return this->nazwa_gracza1;
	}

}
void gracz::wypisz_nick(int liczba_gracza) {
	if (liczba_gracza == 1) {
		cout << this->nazwa_gracza1;
	}
	if (liczba_gracza == 2) {
		cout << this->nazwa_gracza2;
	}
	if (liczba_gracza == 3) {
		cout << this->nazwa_gracza3;
	}

}

void gracz::zapisz_nicki() {
	string g1, g2;
	cout << "Podaj nick pierwszego gracza" << endl;
	cin >> g1;
	cout << "Podaj nick drugiego gracza" << endl;
	cin >> g2;
	set_nick(g1, 1);
	set_nick(g2, 2);
}

void gra::gra_2graczy(char gkod[4], char gkod2[4], char gtab_[4], char gtab_2[4], char gzgadywanie[4], char gzgadywanie2[4]) {

	int liczba_rundy = 1;

	do {
		int ilosc_x = 0;
		int ilosc_x2 = 0;
		int ilosc_o = 0;
		int ilosc_o2 = 0;

		cout << "RUNDA:" << liczba_rundy << endl;
		
		cout << this->nazwa_gracza1 << endl;
		cout << "wpisz kod" << endl;

		//wczytywanie wyboru kolorow gracza
		for (int i = 0; i < 4; i++) {
			char znak;
			do {
				cin >> znak;
			} while (znak != 'Z' && znak != 'N' && znak != 'C' && znak != 'F' && znak != 'M' && znak != 'P');
			gzgadywanie[i] = znak;
		}
		//sprawdzanie ilosci dobrych kolorow na dobrym miejsu
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				if (gzgadywanie[j] == gkod[i] && j == i) {
					ilosc_x++;
				}

			}
		}

		//sprawdzanie ilosci dobrych kolorow na nie swoim miejscu
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				if (gzgadywanie[j] != gkod[j]) {
					if (gzgadywanie[i] != gkod[i]) {
						if (gzgadywanie[j] == gkod[i] && j != i) {
							ilosc_o++;
							j = 4;
						}
					}
				}
			}
		}

		cout << this->nazwa_gracza2 << endl;
		cout << "wpisz kod" << endl;

		//wczytywanie wyboru kolorow gracza 2
		for (int i = 0; i < 4; i++) {
			char znak;
			do {
				cin >> znak;
			} while (znak != 'Z' && znak != 'N' && znak != 'C' && znak != 'F' && znak != 'M' && znak != 'P');
			gzgadywanie2[i] = znak;
		}

		//sprawdzanie ilosci dobrych kolorow na dobrym miejsu 2
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				if (gzgadywanie2[j] == gkod2[i] && j == i) {
					ilosc_x2++;
				}

			}
		}
		//sprawdzanie ilosci dobrych kolorow na nie swoim miejscu 2
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				if (gzgadywanie2[j] != gkod2[j]) {
					if (gzgadywanie2[i] != gkod2[i]) {
						if (gzgadywanie2[j] == gkod2[i] && j != i) {
							ilosc_o2++;
							j = 4;
						}
					}
				}
			}
		}
		uzupelnij_xo(gtab_, ilosc_x, ilosc_o);
		uzupelnij_xo(gtab_2, ilosc_x2, ilosc_o2);

		cout << this->nazwa_gracza1 << "            " << this->nazwa_gracza2 << endl;
		wypisz_kod4(gzgadywanie);
		cout << " ";
		wypisz_(gtab_);
		cout << " ";

		wypisz_kod4(gzgadywanie2);
		cout << " ";
		wypisz_(gtab_2);



		cout << endl << endl;

		liczba_rundy++;
		if (ilosc_x == 4 && ilosc_x2 == ilosc_x) {
			cout << "REMIS!" << endl;
			if (liczba_rundy != 2) {
				cout << "Udalo Wam sie rozwiazac kod w " << liczba_rundy - 1 << " rundach";

			}			if (liczba_rundy == 2) {
				cout << "Udalo Wam sie rozwiazac kod w 1 runde";
			}
			cout << endl;
			liczba_rundy = 11;
			ilosc_x = 5;
		}


		if (ilosc_x == 4) {
			cout << this->nazwa_gracza1;
			cout << " ODGADL KOD!" << endl;
			if (liczba_rundy != 2) {
				cout << "Udalo Ci sie w " << liczba_rundy - 1 << " rundach";

			}			if (liczba_rundy == 2) {
				cout << "Udalo Ci sie w 1 runde";
			}
			cout << endl;
			liczba_rundy = 11;
		}

		if (ilosc_x != 5 && ilosc_x != 4 && liczba_rundy == 11) {
			cout << "Niestety ";
			cout << nazwa_gracza1 << " i " << this->nazwa_gracza2;
			cout << " ,tym razem sie nie udalo" << endl;
			cout << "Kod gracza " << this->nazwa_gracza1 << " : ";
			wypisz_kod4(gkod);
			cout << endl << "Kod gracza " << this->nazwa_gracza2 << " : ";
			wypisz_kod4(gkod2);
			cout << endl;
		}
	} while (liczba_rundy != 11);
}

void gra::gra_2graczy_przeciw_sobie(char gtab_[4], char gtab_2[4], char gzgadywanie[4], char gzgadywanie2[4]) {
	int liczba_rundy = 1;
	char gkod[4];
	char gkod2[4];
	cout << "wpisz kod dla gracza 2" << endl;
	for (int i = 0; i < 4; i++) {
		char znak;
		do {
			cin >> znak;
		} while (znak != 'Z' && znak != 'N' && znak != 'C' && znak != 'F' && znak != 'M' && znak != 'P');
		gkod[i] = znak;
	}
	cout << "wpisz kod dla gracza 1" << endl;
	for (int i = 0; i < 4; i++) {
		char znak;
		do {
			cin >> znak;
		} while (znak != 'Z' && znak != 'N' && znak != 'C' && znak != 'F' && znak != 'M' && znak != 'P');
		gkod2[i] = znak;
	}
	do {
		int ilosc_x = 0;
		int ilosc_x2 = 0;
		int ilosc_o = 0;
		int ilosc_o2 = 0;

		cout << "RUNDA:" << liczba_rundy << endl;
		cout << this->nazwa_gracza1 << endl;
		cout << "wpisz kod" << endl;

		//wczytywanie wyboru kolorow gracza
		for (int i = 0; i < 4; i++) {
			char znak;
			do {
				cin >> znak;
			} while (znak != 'Z' && znak != 'N' && znak != 'C' && znak != 'F' && znak != 'M' && znak != 'P');
			gzgadywanie[i] = znak;
		}
		//sprawdzanie ilosci dobrych kolorow na dobrym miejsu
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				if (gzgadywanie[j] == gkod[i] && j == i) {
					ilosc_x++;
				}

			}
		}

		//sprawdzanie ilosci dobrych kolorow na nie swoim miejscu
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				if (gzgadywanie[j] != gkod[j]) {
					if (gzgadywanie[i] != gkod[i]) {
						if (gzgadywanie[j] == gkod[i] && j != i) {
							ilosc_o++;
							j = 4;
						}
					}
				}
			}
		}

		cout << this->nazwa_gracza2 << endl;
		cout << "wpisz kod" << endl;

		//wczytywanie wyboru kolorow gracza 2
		for (int i = 0; i < 4; i++) {
			char znak;
			do {
				cin >> znak;
			} while (znak != 'Z' && znak != 'N' && znak != 'C' && znak != 'F' && znak != 'M' && znak != 'P');
			gzgadywanie2[i] = znak;
		}

		//sprawdzanie ilosci dobrych kolorow na dobrym miejsu 2
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				if (gzgadywanie2[j] == gkod2[i] && j == i) {
					ilosc_x2++;
				}

			}
		}
		//sprawdzanie ilosci dobrych kolorow na nie swoim miejscu 2
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				if (gzgadywanie2[j] != gkod2[j]) {
					if (gzgadywanie2[i] != gkod2[i]) {
						if (gzgadywanie2[j] == gkod2[i] && j != i) {
							ilosc_o2++;
							j = 4;
						}
					}
				}
			}
		}
		uzupelnij_xo(gtab_, ilosc_x, ilosc_o);
		uzupelnij_xo(gtab_2, ilosc_x2, ilosc_o2);

		cout << this->nazwa_gracza1 << "            " << this->nazwa_gracza2 << endl;
		wypisz_kod4(gzgadywanie);
		cout << " ";
		wypisz_(gtab_);
		cout << " ";

		wypisz_kod4(gzgadywanie2);
		cout << " ";
		wypisz_(gtab_2);



		cout << endl << endl;

		

		liczba_rundy++;
		if (ilosc_x == 4 && ilosc_x2 == ilosc_x) {
			cout << "REMIS!" << endl;
			if (liczba_rundy != 2) {
				cout << "Udalo Wam sie rozwiazac kod w " << liczba_rundy - 1 << " rundach";

			}			if (liczba_rundy == 2) {
				cout << "Udalo Wam sie rozwiazac kod w 1 runde";
			}
			cout << endl;
			liczba_rundy = 11;
			ilosc_x = 5;
			
		}


		if (ilosc_x == 4) {
			cout << this->nazwa_gracza1;
			cout << " ODGADL KOD!" << endl;
			if (liczba_rundy != 2) {
				cout << "Udalo Ci sie w " << liczba_rundy - 1 << " rundach";

			}			if (liczba_rundy == 2) {
				cout << "Udalo Ci sie w 1 runde";
			}
			cout << endl;
			liczba_rundy = 11;
		}
		
		if (ilosc_x != 5 && ilosc_x != 4 && liczba_rundy == 11) {
				cout << "Niestety ";
				cout << this->nazwa_gracza1 << " i " << this->nazwa_gracza2;
				cout << " ,tym razem sie nie udalo" << endl;
				cout << "Kod gracza " << this->nazwa_gracza1 << " : ";
				wypisz_kod4(gkod);
				cout << endl << "Kod gracza " << this->nazwa_gracza2 << " : ";
				wypisz_kod4(gkod2);
				cout << endl;
		}
	} while (liczba_rundy != 11);
}