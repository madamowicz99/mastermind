#include "gui.h"
#include "game1.h"
#include "cstdlib"
#include "windows.h"
#include "stdlib.h"


int main() {
	gui gui;
	gra gra1;
	gra gracz1, gracz2, gracz3;

	srand((unsigned)time(NULL));

	gui.info_gry();

	gra1.uzupelnij_(gra1.kod);
	gra1.uzupelnij_(gra1.tab_);
	gra1.utworz_kod(gra1.baza, gra1.kod);
	gra1.wypisz_kod4(gra1.kod);
	
	gracz1.uzupelnij_(gracz1.gtab_);
	gracz1.utworz_kod(gracz1.baza, gracz1.gkod);
	gracz1.wypisz_kod4(gracz1.gkod);

	cout << "+++";

	gracz2.uzupelnij_(gracz2.gtab_2);
	gracz2.utworz_kod(gracz2.baza, gracz2.gkod2);
	gracz2.wypisz_kod4(gracz2.gkod2);
	
	cout << endl << endl;

	gracz3.uzupelnij_(gracz3.gtab_);
	gracz3.uzupelnij_(gracz3.gtab_2);
	
	int wybor;
	
	do {
		gui.info_tryb();
		
		wybor = gui.wybierz_tryb();
		switch (wybor) {
		case 1: {
			gra1.gra_z_komputerem(gra1.kod, gra1.tab_, gra1.zgadywanie);

			break;
		}
		case 2: {
			gracz1.zapisz_nicki();
			gracz1.gra_2graczy(gracz1.gkod, gracz2.gkod2, gracz1.gtab_, gracz2.gtab_2, gracz1.gzgadywanie, gracz2.gzgadywanie2);
			break;
		}
		case 3: {
			gracz1.zapisz_nicki();
			gracz3.gra_2graczy_przeciw_sobie(gracz3.gtab_, gracz3.gtab_2, gracz3.gzgadywanie, gracz3.gzgadywanie2);
			break;
		}
		case 4: {
			break;
		}
		}
	} while (wybor != 4);
	
	return 0;
}